package com.kleinanzeigen.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class ExampleController {

    @GetMapping("/hello")
    public String helloWorld(){
        return "Hello world";
    }
}
