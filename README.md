# Kleinanzeigen

Entwicklung einer Online-Plattform für Kleinanzeigen. Der Dienst ermöglicht das kostenlose Anlegen und Suchen von Geboten und Gesuchen von Gebrauchsgegenständen oder WG-Zimmern.